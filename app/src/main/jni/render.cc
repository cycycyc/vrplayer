//
// Created by chenyc on 17-1-2.
//

#include "render.h"
#include <android/log.h>

#define LOG_TAG "NativeRender"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGW(...) \
      __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

Render::Render(JNIEnv* env, jlong gvr_context_ptr)
        :  // This is the GVR context pointer obtained from Java:
        gvr_context_(reinterpret_cast<gvr_context*>(gvr_context_ptr)),
        // Wrap the gvr_context* into a GvrApi C++ object for convenience:
        gvr_api_(gvr::GvrApi::WrapNonOwned(gvr_context_)),
        gvr_api_initialized_(false),
        viewport_list_(gvr_api_->CreateEmptyBufferViewportList()),
        scratch_viewport_(gvr_api_->CreateBufferViewport()) {
    LOGD("Render initialized.");
}

Render::~Render() {
    LOGD("Render shutdown.");
}

void Render::OnResume() {
    LOGD("Render::OnResume");
    if (gvr_api_initialized_) {
        gvr_api_->ResumeTracking();
    }
}

void Render::OnPause() {
    LOGD("Render::OnPause");
    if (gvr_api_initialized_) gvr_api_->PauseTracking();
}

void Render::OnSurfaceCreated() {
    LOGD("DemoApp::OnSurfaceCreated");

    LOGD("Initializing GL on GvrApi.");
    gvr_api_->InitializeGl();

    LOGD("Initializing framebuffer.");
    std::vector<gvr::BufferSpec> specs;
    specs.push_back(gvr_api_->CreateBufferSpec());
    framebuf_size_ = gvr_api_->GetMaximumEffectiveRenderTargetSize();
    // Because we are using 2X MSAA, we can render to half as many pixels and
    // achieve similar quality. Scale each dimension by sqrt(2)/2 ~= 7/10ths.
    framebuf_size_.width = (7 * framebuf_size_.width) / 10;
    framebuf_size_.height = (7 * framebuf_size_.height) / 10;

    specs[0].SetSize(framebuf_size_);
    specs[0].SetColorFormat(GVR_COLOR_FORMAT_RGBA_8888);
    specs[0].SetDepthStencilFormat(GVR_DEPTH_STENCIL_FORMAT_DEPTH_16);
    specs[0].SetSamples(2);
    swapchain_.reset(new gvr::SwapChain(gvr_api_->CreateSwapChain(specs)));

    gvr_api_initialized_ = true;

    LOGD("Init complete.");
}

void Render::OnSurfaceChanged(int width, int height) {
    LOGD("Render::OnSurfaceChanged %dx%d", width, height);
}

void Render::OnDrawFrame() {

}
