//
// Created by chenyc on 17-1-2.
//

#ifndef VRPLAYER_RENDER_H
#define VRPLAYER_RENDER_H

#include <jni.h>
#include "vr/gvr/capi/include/gvr.h"

class Render {
public:
    Render(JNIEnv* env, jlong gvr_context_ptr);
    ~Render();
    void OnResume();
    void OnPause();
    void OnSurfaceCreated();
    void OnSurfaceChanged(int width, int height);
    void OnDrawFrame();

private:

    // Gvr API entry point.
    gvr_context* gvr_context_;
    std::unique_ptr<gvr::GvrApi> gvr_api_;
    bool gvr_api_initialized_;

    // Handle to the swapchain. On every frame, we have to check if the buffers
    // are still the right size for the frame (since they can be resized at any
    // time). This is done by PrepareFramebuffer().
    std::unique_ptr<gvr::SwapChain> swapchain_;

    // List of rendering params (used to render each eye).
    gvr::BufferViewportList viewport_list_;
    gvr::BufferViewport scratch_viewport_;

    // Size of the offscreen framebuffer.
    gvr::Sizei framebuf_size_;

    // Disallow copy and assign.
    Render(const Render& other) = delete;
    Render& operator=(const Render& other) = delete;
};


#endif //VRPLAYER_RENDER_H
