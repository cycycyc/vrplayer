
//
// Created by cheny on 1/9/2017.
//

#ifndef VRPLAYER_DEMUXER_H
#define VRPLAYER_DEMUXER_H

#include <jni.h>
extern "C" {
#include "libavformat/avformat.h"
};

class Demuxer {
public:
    Demuxer(JNIEnv* env, jstring url_jstr);
    ~Demuxer();

    jbyteArray Demux();

private:
    JNIEnv* env_;
    AVFormatContext* ctx_;
    AVPacket pkt_;

    Demuxer(const Demuxer& other) = delete;
    Demuxer& operator=(const Demuxer& other) = delete;
};

#endif //VRPLAYER_DEMUXER_H
