//
// Created by cheny on 1/9/2017.
//

#include "demuxer.h"
#include <android/log.h>

#define LOG_TAG "NativeDemuxer"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGW(...) \
      __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

Demuxer::Demuxer(JNIEnv* env, jstring url_jstr) : env_(env), ctx_(NULL) {
    LOGD("Demuxer initialized.");

    const char *url = env->GetStringUTFChars(url_jstr, 0);

    av_register_all();
    AVDictionary* dict = NULL;
    av_dict_set(&dict, "rtsp_transport", "tcp", 0);
    if (avformat_open_input(&ctx_, url, NULL, &dict) != 0) {
        LOGE("avformat_open_input failed");
    }
    if (avformat_find_stream_info(ctx_, NULL) != 0) {
        LOGE("avformat_find_stream_info failed");
    }

    env->ReleaseStringUTFChars(url_jstr, url);
}

Demuxer::~Demuxer() {
    LOGD("Render shutdown.");

    if (ctx_ != NULL) {
        avformat_close_input(&ctx_);
    }
}

jbyteArray Demuxer::Demux() {
    jbyteArray jbArray = NULL;

    av_init_packet(&pkt_);
    if (av_read_frame(ctx_, &pkt_) == 0) {
        jbArray = env_->NewByteArray(pkt_.size);
        env_->SetByteArrayRegion(jbArray, 0, pkt_.size, (const jbyte*)pkt_.data);
        av_packet_unref(&pkt_);
    }

    return jbArray;
}