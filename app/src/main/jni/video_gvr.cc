#include <jni.h>
#include <string>
#include "render.h"
#include "demuxer.h"

#define NATIVE_METHOD(return_type, method_name) \
  JNIEXPORT return_type JNICALL                 \
      Java_com_cyc_vrplayer_MainActivity_##method_name

extern "C" {

NATIVE_METHOD(jlong, nativeOnCreate)
(JNIEnv* env, jobject obj, jlong gvrContextPtr);
NATIVE_METHOD(void, nativeOnResume)
(JNIEnv* env, jobject obj, jlong native_render_jptr);
NATIVE_METHOD(void, nativeOnPause)
(JNIEnv* env, jobject obj, jlong native_render_jptr);
NATIVE_METHOD(void, nativeOnSurfaceCreated)
(JNIEnv* env, jobject obj, jlong native_render_jptr);
NATIVE_METHOD(void, nativeOnSurfaceChanged)
(JNIEnv* env, jobject obj, jint width, jint height,
 jlong native_render_jptr);
NATIVE_METHOD(void, nativeOnDrawFrame)
(JNIEnv* env, jobject obj, jlong native_render_jptr);
NATIVE_METHOD(void, nativeOnDestroy)
(JNIEnv* env, jobject obj, jlong native_render_jptr);

NATIVE_METHOD(jlong, nativeDemuxerOpen)
(JNIEnv* env, jobject obj, jstring url_jstr);
NATIVE_METHOD(void, nativeDemuxerClose)
(JNIEnv* env, jobject obj, jlong native_demuxer_jptr);
NATIVE_METHOD(jbyteArray, nativeDemuxerDemux)
(JNIEnv* env, jobject obj, jlong native_demuxer_jptr);

}

namespace {
    template <typename T>
    inline jlong jptr(T* ptr) { return reinterpret_cast<intptr_t>(ptr); }

    template <typename T>
    inline T* ptr(jlong jptr) { return reinterpret_cast<T*>(jptr); }
}  // namespace

NATIVE_METHOD(jlong, nativeOnCreate)
        (JNIEnv* env, jobject obj, jlong gvr_context_ptr) {
    return jptr(new Render(env, gvr_context_ptr));
}

NATIVE_METHOD(void, nativeOnResume)
        (JNIEnv* env, jobject obj, jlong native_render_jptr) {
    ptr<Render>(native_render_jptr)->OnResume();
}

NATIVE_METHOD(void, nativeOnPause)
        (JNIEnv* env, jobject obj, jlong native_render_jptr) {
    ptr<Render>(native_render_jptr)->OnPause();
}

NATIVE_METHOD(void, nativeOnSurfaceCreated)
        (JNIEnv* env, jobject obj, jlong native_render_jptr) {
    ptr<Render>(native_render_jptr)->OnSurfaceCreated();
}

NATIVE_METHOD(void, nativeOnSurfaceChanged)
        (JNIEnv* env, jobject obj, jint width, jint height,
         jlong native_render_jptr) {
    ptr<Render>(native_render_jptr)
            ->OnSurfaceChanged(static_cast<int>(width), static_cast<int>(height));
}

NATIVE_METHOD(void, nativeOnDrawFrame)
        (JNIEnv* env, jobject obj, jlong native_render_jptr) {
    ptr<Render>(native_render_jptr)->OnDrawFrame();
}

NATIVE_METHOD(void, nativeOnDestroy)
        (JNIEnv* env, jobject obj, jlong native_render_jptr) {
    delete ptr<Render>(native_render_jptr);
}

NATIVE_METHOD(jlong, nativeDemuxerOpen)
        (JNIEnv* env, jobject obj, jstring url_jstr) {
    return jptr(new Demuxer(env, url_jstr));
}

NATIVE_METHOD(void, nativeDemuxerClose)
        (JNIEnv* env, jobject obj, jlong native_demuxer_jptr) {
    delete ptr<Demuxer>(native_demuxer_jptr);
}

NATIVE_METHOD(jbyteArray, nativeDemuxerDemux)
        (JNIEnv* env, jobject obj, jlong native_demuxer_jptr) {
    return ptr<Demuxer>(native_demuxer_jptr)->Demux();
}