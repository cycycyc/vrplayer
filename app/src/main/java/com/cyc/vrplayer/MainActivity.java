package com.cyc.vrplayer;

import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Surface;

import com.google.vr.sdk.base.GvrActivity;
import com.google.vr.sdk.base.GvrView;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MainActivity extends GvrActivity implements Renderer.OnSurfaceReadyListener {

    static {
        System.loadLibrary("video_gvr");
    }

    private static final String TAG = "MainActivity";
    private static final boolean USE_FFMPEG_DEMUXER = true;
//    private static final String URL = "/sdcard/miss-zhujiang.ts";
//    private static final String URL = "rtsp://192.168.1.7:8001/test";
//    private static final String URL = "rtsp://192.168.1.9:80/test";

    private String uri;
    private boolean isStereo;

    private GvrView gvrView;
    private Renderer renderer;
    private MediaPlayer mediaPlayer;
    private MediaCodec mediaCodec;
    private long nativeDemuxer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        uri = intent.getStringExtra("uri");
        isStereo = intent.getBooleanExtra("isStereo", false);

        initializeGvrView();

        if (!USE_FFMPEG_DEMUXER) {
            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(uri);
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                mediaCodec = MediaCodec.createDecoderByType("video/avc");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void OnSurfaceReady(Surface surface) {
        if (!USE_FFMPEG_DEMUXER) {
            mediaPlayer.setSurface(surface);
        } else {
            mediaCodec.configure(MediaFormat.createVideoFormat("video/avc", 0, 0), surface, null, 0);
            mediaCodec.start();

            new Thread(new Runnable() {

                @Override
                public void run() {
                    nativeDemuxer = nativeDemuxerOpen(uri);

                    while (true) {
                        byte[] frame = nativeDemuxerDemux(nativeDemuxer);
                        if (frame != null && frame.length > 0) {
                            Log.d(TAG, "frame: " + frame.length);
                            int inputBufferId = mediaCodec.dequeueInputBuffer(-1);
                            if (inputBufferId >= 0) {
                                ByteBuffer inputBuffer = mediaCodec.getInputBuffer(inputBufferId);
                                inputBuffer.clear();
                                inputBuffer.put(frame);
                                mediaCodec.queueInputBuffer(inputBufferId, 0, frame.length, 0, 0);
                            }
                        } else {
                            break;
                        }
                    }

                    mediaCodec.stop();
                    mediaCodec.release();
                    nativeDemuxerClose(nativeDemuxer);
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
                    try {
                        while (true) {
                            int outputBufferId = mediaCodec.dequeueOutputBuffer(info, -1);
                            if (outputBufferId >= 0) {
                                mediaCodec.releaseOutputBuffer(outputBufferId, true);
                            } else if (outputBufferId == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                                MediaFormat format = mediaCodec.getOutputFormat();
                                Log.d(TAG, "output format changed: " + format);
                            } else if (outputBufferId == MediaCodec.INFO_TRY_AGAIN_LATER) {
                                Log.d(TAG, "try again later");
                            } else if (outputBufferId == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                                Log.d(TAG, "output buffers changed");
                            } else {
                                Log.d(TAG, "output unexpected " + outputBufferId);
                                break;
                            }
                        }
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    public void initializeGvrView() {
        setContentView(R.layout.activity_main);

        gvrView = (GvrView) findViewById(R.id.gvrView);
        gvrView.setEGLConfigChooser(8, 8, 8, 8, 16, 8);

        renderer = new Renderer(this, this, isStereo);
        gvrView.setRenderer(renderer);
        gvrView.setTransitionViewEnabled(true);

        // Enable Cardboard-trigger feedback with Daydream headsets. This is a simple way of supporting
        // Daydream controller input for basic interactions using the existing Cardboard trigger API.
        gvrView.enableCardboardTriggerEmulation();

        if (gvrView.setAsyncReprojectionEnabled(true)) {
            // Async reprojection decouples the app framerate from the display framerate,
            // allowing immersive interaction even at the throttled clockrates set by
            // sustained performance mode.
            com.google.vr.sdk.base.AndroidCompat.setSustainedPerformanceMode(this, true);
        }

        setGvrView(gvrView);
    }

    private native long nativeOnCreate(long gvrContextPtr);
    private native void nativeOnResume(long nativeRender);
    private native void nativeOnPause(long nativeRender);
    private native void nativeOnSurfaceCreated(long nativeRender);
    private native void nativeOnSurfaceChanged(int width, int height, long nativeRender);
    private native void nativeOnDrawFrame(long nativeRender);
    private native void nativeOnDestroy(long nativeRender);

    private native long nativeDemuxerOpen(String url);
    private native byte[] nativeDemuxerDemux(long nativeDemuxer);
    private native void nativeDemuxerClose(long nativeDemuxer);
}
