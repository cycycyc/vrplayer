package com.cyc.vrplayer;

import android.opengl.GLES20;
import android.util.Log;

/**
 * Created by cheny on 1/9/2017.
 */

public class ShaderProgram {
    public static final String TAG = "ShaderProgram";

    private int shaderProgramHandle;

    public ShaderProgram(String vertexShader, String fragmentShader) {
        shaderProgramHandle = createProgram(vertexShader, fragmentShader);
    }

    public int getShaderHandle() {
        return shaderProgramHandle;
    }

    public void release() {
        GLES20.glDeleteProgram(shaderProgramHandle);
        shaderProgramHandle = -1;
    }

    public int getAttribute(String name) {
        return GLES20.glGetAttribLocation(shaderProgramHandle, name);
    }

    public int getUniform(String name) {
        return GLES20.glGetUniformLocation(shaderProgramHandle, name);
    }

    private static int createProgram(String vertexSource, String fragmentSource) {
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
        int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
        int program = GLES20.glCreateProgram();
        GLHelper.checkGlError("glCreateProgram");
        GLES20.glAttachShader(program, vertexShader);
        GLHelper.checkGlError("glAttachShader");
        GLES20.glAttachShader(program, pixelShader);
        GLHelper.checkGlError("glAttachShader");
        GLES20.glLinkProgram(program);
        GLHelper.checkGlError("glLinkProgram");
        return program;
    }

    private static int loadShader(int shaderType, String source) {
        int shader = GLES20.glCreateShader(shaderType);
        GLHelper.checkGlError("glCreateShader");
        GLES20.glShaderSource(shader, source);
        GLES20.glCompileShader(shader);
        GLHelper.checkGlError("glCompileShader");
        return shader;
    }
}
