package com.cyc.vrplayer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class OpenActivity extends AppCompatActivity {

    EditText mEditText;
    TextView mTextView;
    ToggleButton mToggleButton;
    Button mButton;

    String uri;
    boolean isStereo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open);

        mEditText = (EditText)findViewById(R.id.editText);
        mTextView = (TextView)findViewById(R.id.textView);
        mToggleButton = (ToggleButton)findViewById(R.id.toggleButton);
        mButton = (Button)findViewById(R.id.button);

        final SharedPreferences sharedPreferences = getSharedPreferences("vrplayer", MODE_PRIVATE);
        uri = sharedPreferences.getString("uri", "rtsp://192.168.1.9:80/test");
        isStereo = sharedPreferences.getBoolean("stereo", false);
        mEditText.setText(uri);
        if (isStereo) {
            mTextView.setText("Stereo");
        } else {
            mTextView.setText("Mono");
        }

        mToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mTextView.setText("Stereo");
                } else {
                    mTextView.setText("Mono");
                }
                isStereo = isChecked;
            }
        });

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putString("uri", mEditText.getText().toString()).putBoolean("isStereo", isStereo).commit();

                Intent intent = new Intent(OpenActivity.this, MainActivity.class);
                intent.putExtra("isStereo", isStereo);
                intent.putExtra("uri", mEditText.getText().toString());
                startActivity(intent);
            }
        });
    }
}
