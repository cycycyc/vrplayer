package com.cyc.vrplayer;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.view.Surface;

import com.google.vr.sdk.base.Eye;
import com.google.vr.sdk.base.GvrView;
import com.google.vr.sdk.base.HeadTransform;
import com.google.vr.sdk.base.Viewport;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.microedition.khronos.egl.EGLConfig;

/**
 * Created by cheny on 1/9/2017.
 */

public class Renderer implements GvrView.StereoRenderer, SurfaceTexture.OnFrameAvailableListener {

    interface OnSurfaceReadyListener {
        void OnSurfaceReady(Surface surface);
    }

    Context context;
    OnSurfaceReadyListener onSurfaceReadyListener;
    StereoSphere sphere;
    SurfaceTexture surfaceTexture;
    int textureId = -1;
    boolean updateTexture = false;

    private float[] textureMatrix = GLHelper.createIdentityMtx();
    private float[] modelMatrix =  GLHelper.createIdentityMtx();
    private float[] pvMatrix =  GLHelper.createIdentityMtx();
    private float[] perspective =  GLHelper.createIdentityMtx();
    private float[] view =  GLHelper.createIdentityMtx();
    private float[] mvpMatrix =  GLHelper.createIdentityMtx();

    private static final float Z_NEAR = 1f;
    private static final float Z_FAR = 1000.0f;

    private ShaderProgram shaderProgram;
    private int aPositionLocation;
    private int uMVPMatrixLocation;
    private int uTextureMatrixLocation;
    private int aTextureCoordLocation;

    private boolean isStereo;

    public Renderer(Context context, OnSurfaceReadyListener onSurfaceReadyListener, boolean isStereo) {
        this.context = context;
        this.onSurfaceReadyListener = onSurfaceReadyListener;
        this.isStereo = isStereo;
        this.sphere = new StereoSphere(100f, 90, 180, isStereo);

        Matrix.setIdentityM(view, 0);
    }

    @Override
    public synchronized void onFrameAvailable(SurfaceTexture surfaceTexture) {
        updateTexture = true;
    }

    @Override
    public void onNewFrame(HeadTransform headTransform) {
        view = headTransform.getHeadView();
    }

    @Override
    public void onDrawEye(Eye eye) {
        int eyeIndex = isStereo ? (eye.getType() == Eye.Type.RIGHT ? 1 : 0) : 0;

        if (updateTexture) {
            surfaceTexture.updateTexImage();
            updateTexture = false;
        }
        surfaceTexture.getTransformMatrix(textureMatrix);

        perspective  = eye.getPerspective(Z_NEAR, Z_FAR);
        Matrix.multiplyMM(pvMatrix, 0, perspective, 0, view, 0);
        Matrix.multiplyMM(mvpMatrix, 0, pvMatrix, 0, modelMatrix, 0);

        GLES20.glClearColor(0.0f,0.0f,0.0f,1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        GLES20.glUseProgram(shaderProgram.getShaderHandle());

        GLES20.glEnableVertexAttribArray(aPositionLocation);
        GLHelper.checkGlError("glEnableVertexAttribArray");

        GLES20.glVertexAttribPointer(aPositionLocation, 3,
                GLES20.GL_FLOAT, false, 0, sphere.getVertices());
        GLHelper.checkGlError("glVertexAttribPointer");

        GLES20.glEnableVertexAttribArray(aTextureCoordLocation);
        GLHelper.checkGlError("glEnableVertexAttribArray");

        GLES20.glVertexAttribPointer(aTextureCoordLocation, 2,
                GLES20.GL_FLOAT, false, 0, sphere.getTexCoords(eyeIndex));
        GLHelper.checkGlError("glVertexAttribPointer");

        Matrix.translateM(textureMatrix, 0, 0, 1, 0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId);
        GLES20.glUniformMatrix4fv(uTextureMatrixLocation, 1, false, textureMatrix, 0);
        GLES20.glUniformMatrix4fv(uMVPMatrixLocation, 1, false, mvpMatrix, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES,
                sphere.getNumIndices(), GLES20.GL_UNSIGNED_SHORT, sphere.getIndices());
    }

    @Override
    public void onFinishFrame(Viewport viewport) {

    }

    @Override
    public void onSurfaceChanged(int width, int height) {
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onSurfaceCreated(EGLConfig eglConfig) {
        onSurfaceReadyListener.OnSurfaceReady(getVideoDecodeSurface());
        Matrix.setRotateM(modelMatrix, 0, -90.0f, 1, 0, 0);

        shaderProgram = new ShaderProgram(
                readRawTextFile(context, R.raw.video_vertex_shader),
                readRawTextFile(context, R.raw.video_fragment_shader));
        aPositionLocation = shaderProgram.getAttribute("aPosition");
        uMVPMatrixLocation = shaderProgram.getUniform("uMVPMatrix");
        aTextureCoordLocation = shaderProgram.getAttribute("aTextureCoord");
        uTextureMatrixLocation = shaderProgram.getUniform("uTextureMatrix");
    }

    @Override
    public void onRendererShutdown() {

    }

    private Surface getVideoDecodeSurface() {
        textureId = GLHelper.generateExternalTexture();
        surfaceTexture = new SurfaceTexture(textureId);

        surfaceTexture.setOnFrameAvailableListener(this);
        return new Surface(surfaceTexture);
    }

    public static String readRawTextFile(Context context, int resId) {
        InputStream is = context.getResources().openRawResource(resId);
        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader buf = new BufferedReader(reader);
        StringBuilder text = new StringBuilder();
        try {
            String line;
            while ((line = buf.readLine()) != null) {
                text.append(line).append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }
}
