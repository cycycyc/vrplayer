package com.cyc.vrplayer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by cheny on 1/9/2017.
 */

public class StereoSphere {

    private FloatBuffer mVertices;
    private FloatBuffer[] mTexCoords = new FloatBuffer[2];
    private ShortBuffer mIndices;
    private int mNumIndices;

    public FloatBuffer getVertices() {
        return mVertices;
    }
    public FloatBuffer getTexCoords(int eye) { return mTexCoords[eye]; }
    public ShortBuffer getIndices() {
        return mIndices;
    }
    public int getNumIndices() {
        return mNumIndices;
    }

    public StereoSphere(float radius, int rings, int sectors, boolean isStereo) {
        int iMax = rings + 1;
        int jMax = sectors + 1;
        int nVertices = iMax * jMax;
        float angleStepI = ((float) Math.PI / rings);
        float angleStepJ = ((2.0f * (float) Math.PI) / sectors);

        // 3 vertex coords + 2 texture coords
        mVertices = ByteBuffer.allocateDirect(nVertices * 3 * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTexCoords[0] = ByteBuffer.allocateDirect(nVertices * 2 * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mTexCoords[1] = ByteBuffer.allocateDirect(nVertices * 2 * 4)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();
        mIndices = ByteBuffer.allocateDirect(sectors * rings * 6 * 2)
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        mNumIndices = sectors * rings * 6;

        float[] vertices = new float[nVertices * 3];
        float[] texcoords = new float[nVertices * 2];
        float[] texcoords2 = new float[nVertices * 2];
        int v = 0, t = 0;
        for (int i = 0; i < iMax; i++) {
            for (int j = 0; j < jMax; j++) {
                float sini = (float) Math.sin(angleStepI * i);
                float sinj = (float) Math.sin(angleStepJ * j);
                float cosi = (float) Math.cos(angleStepI * i);
                float cosj = (float) Math.cos(angleStepJ * j);
                // vertex x,y,z
                vertices[v++] = radius * sini * sinj;
                vertices[v++] = radius * sini * cosj;
                vertices[v++] = radius * cosi;
                // texture s,t
                texcoords[t] = (float) j / (float) sectors;
                if (isStereo) {
                    texcoords2[t] = (float) j / (float) sectors;
                }
                t++;
                if (isStereo) {
                    texcoords[t] = (1.0f - i) / (float) rings / 2.f;
                    texcoords2[t] = (1.0f - i) / (float) rings / 2.f - 0.5f;
                } else {
                    texcoords[t] = (1.0f - i) / (float) rings;
                }
                t++;
            }
        }
        mVertices.put(vertices);
        mVertices.position(0);
        mTexCoords[0].put(texcoords);
        mTexCoords[0].position(0);
        if (isStereo) {
            mTexCoords[1].put(texcoords2);
            mTexCoords[1].position(0);
        }

        short[] indexBuffer = new short[mNumIndices];
        int index = 0;
        for (int i = 0; i < rings; i++) {
            for (int j = 0; j < sectors; j++) {
                int i1 = i + 1;
                int j1 = j + 1;
                indexBuffer[index++] = (short) (i * jMax + j);
                indexBuffer[index++] = (short) (i1 * jMax + j);
                indexBuffer[index++] = (short) (i1 * jMax + j1);
                indexBuffer[index++] = (short) (i * jMax + j);
                indexBuffer[index++] = (short) (i1 * jMax + j1);
                indexBuffer[index++] = (short) (i * jMax + j1);
            }
        }
        mIndices.put(indexBuffer);
        mIndices.position(0);
    }
}
